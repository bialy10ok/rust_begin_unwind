#![crate_type="staticlib"]
#![no_std]
#![no_builtins]

/*
pub extern fn panic_fmt(_: ::core::fmt::Arguments, _: &'static str, _: u32) -> ! {
    loop {}
} */

#[panic_handler]
pub extern "C" fn panic_handler(_info: &core::panic::PanicInfo) -> !
{
 loop {}
}

#[no_mangle]
pub extern "C" fn suma(a: i32, b: i32) -> i32
{
 a + b
}
