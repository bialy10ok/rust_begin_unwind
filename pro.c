#include <stdio.h> /* dla printf */
#include <stdint.h>
#include <inttypes.h>

extern int32_t suma(int32_t, int32_t);

int main(void)
{
int32_t wynik = suma(1, 2);

 printf("Wynik %d\n", wynik);
 /* fflush(STDOUT); */

return 0;
}
