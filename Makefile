OPCJE_R = --print native-static-libs -C panic=abort -C debuginfo=0 \
          -C debug-assertions=off -C overflow-checks=no \
          -C link-arg=-nostartfiles

OPCJE_C = -Wall -Werror -pedantic -pedantic-errors -std=c99

pro: obj/pro.o obj/bib.o
	clang $(OPCJE_C) -o pro obj/bib.o obj/pro.o

#tu zakładamy aktywne kasowanie innych funkcji z src/bib.ll
prozll: obj/pro.o obj/zllbib.o
	clang $(OPCJE_C) -o prozll obj/zllbib.o obj/pro.o

src/bib.ll: src/bib.rs Makefile Cargo.toml
	rustc --emit=llvm-ir --print native-static-libs -C panic=abort -C debuginfo=0\
	 -C debug-assertions=off -C link-arg=-nostartfiles\
	 src/bib.rs -o $@

obj/bib.o: src/bib.rs Makefile Cargo.toml
	@mkdir -p obj
	rustc --emit=obj -C panic=abort -C debuginfo=0 -C debug-assertions=off \
         -C overflow-checks=no -C link-arg=-nostartfiles src/bib.rs -o obj/bib.o

obj/pro.o: pro.c
	@mkdir -p obj
	clang $(OPCJE_C) -c pro.c -o obj/pro.o

#zobacz jak wygląda .obj
poka: obj/bib.o src/bib.ll
	readelf --syms ./obj/bib.o

obj/zllbib.o: src/bib.ll
	clang -c src/bib.ll -o obj/zllbib.o
	readelf --syms ./obj/zllbib.o

clean:
	rm -f ./pro ./prozll ./obj/*.o ./obj/*.a ./src/*.ll
